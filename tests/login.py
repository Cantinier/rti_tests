import requests


def login_test(login, password):
    return requests.post("https://mantisbt.org/bugs/login.php",
                         data={'username': login, 'password': password})


def login_correct_login_pass(all_t_k, bugs):
    login = 'chernyy_pg'
    password = 'ohogut03'
    request = login_test(login, password)
    if request.url == 'https://mantisbt.org/bugs/my_view_page.php':
        all_t_k.append(True)
    else:
        bugs.append('Тесткейс корректной авторизации не пройден. Логин: {}, пароль:{}'.format(login, password))
        all_t_k.append(False)


def login_incorrect_pass(all_t_k, bugs):
    login = 'chernyy_pg'
    password = 'ohogut04'
    request = login_test(login, password)
    if request.url == 'https://mantisbt.org/bugs/my_view_page.php':
        all_t_k.append(False)
        bugs.append('Тесткейс некорректного пароля не пройден. Логин: {}, пароль:{}'.format(login, password))
    else:
        all_t_k.append(True)


def login_incorrect_login(all_t_k, bugs):
    login = 'chernyy_p_g'
    password = 'ohogut03'
    request = login_test(login, password)
    if request.url == 'https://mantisbt.org/bugs/my_view_page.php':
        all_t_k.append(False)
        bugs.append('Тесткейс некорректного пароля не пройден. Логин: {}, пароль:{}'.format(login, password))
    else:
        all_t_k.append(True)
