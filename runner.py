from tests.login import *
from tests.reg import *

all_t_k = list()
bugs = list()

login_correct_login_pass(all_t_k, bugs)
login_incorrect_pass(all_t_k, bugs)
login_incorrect_login(all_t_k, bugs)
reg_test()


if all(all_t_k):
    print('Тестирование пройдено успешно')
else:
    print('В процессе тестирования обнаружены следующие ошибки')
    for row in bugs:
        print(row)
